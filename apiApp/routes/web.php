<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "nada por aca!";
});

$router->get('/info', function () {
    return 'schema';
});

/*	querys		*/
$router->group(['prefix' => 'query'], function () use ($router) {
	//Metadatos
	//Proyectos
	//Votos
});
/*	records		*/
$router->group(['prefix' => 'records'], function () use ($router) {
	//Proyectos / Iniciativas
	//Votos
  $router->get('/vote/{id}', ['middleware' => 'auth', function (Request $request, $id) {
    $user = Auth::user();
    $user = $request->user();
  }]);
});
/*	administrator	*/
$router->group(['prefix' => 'admin'], function () use ($router) {
	//Entidades
	//Usuarios
  $router->get('/', function () {
    return response()->json(['name' => 'WILX', 'state' => 'SV']);
  });
});
